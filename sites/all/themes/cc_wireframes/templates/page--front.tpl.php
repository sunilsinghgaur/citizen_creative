<?php
/**
 * @file
 * @ingroup themeable
 */
global $base_url;
unset($page['content']['system_main']['default_message']);
?>
<div class="topNav">
   <div class="container">
       <div class="row">
         <?php if (!empty($page['top_navigation'])): ?>
         <?php print render($page['top_navigation']); ?>
         <?php endif; ?>
       </div>
    </div>     
</div>

<header id="navbar" role="banner" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php echo $base_url; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
    </div>
    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse pull-right">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</header>
<div class="first-container">
  <?php if (!empty($site_slogan)): ?>
    <p class="lead"><?php print $site_slogan; ?></p>
  <?php endif; ?>
  <?php print render($page['header']); ?>
  <div>
    <?php if (!empty($page['highlighted'])): ?>
      <div class="highlighted_jumbotron"><?php print render($page['highlighted']); ?></div>
    <?php endif; ?>
    <?php
    if (!empty($breadcrumb)): print $breadcrumb;
    endif;
    ?>
    <?php print render($title_prefix); ?>
    <!--<?php if (!empty($title)): ?>
      <h1 class="page-header"><?php //print $title; ?></h1>
    <?php endif; ?>-->
    <?php print render($title_suffix); ?>
    <?php print $messages; ?>
    <?php if (!empty($tabs)): ?>
      <?php //print render($tabs); ?>
    <?php endif; ?>
    <?php if (!empty($page['help'])): ?>
      <?php print render($page['help']); ?>
    <?php endif; ?>
    <?php print render($page['content']); ?>
    </div>
 </div>

<div class="second-container">
  <div class="container">
    <div class="row">
      <?php if (!empty($page['content_two'])): ?>
        <?php print render($page['content_two']); ?>
      <?php endif; ?>
    </div></div></div>
<div class="third-container">
  <div class="container">
    <div class="row">
    <div class="view-testimonial-all">
      <?php if (!empty($page['content_third'])): ?>
        <?php print render($page['content_third']); ?>
      <?php endif; ?>
    </div></div></div></div>
<div class="fourth-container">
  <div class="container">
    <div class="row">
      <?php if (!empty($page['content_fourth'])): ?>
        <?php print render($page['content_fourth']); ?>
      <?php endif; ?>
    </div></div></div>

<footer class="footer">
  <div class="container">
    <div class="row">
  <?php print render($page['footer']); ?>
  <?php print render($page['footer_two']); ?>
  </div></div>
</footer>

<div class="footer-third">
  <div class="container">
    <div class="row">
  <?php print render($page['footer_third']); ?>
  </div></div>
</div>
